/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatcliente;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextArea;

/**
 *
 * @author KONET
 */
public class ThreadChat extends Thread {

    private JTextArea chat; //Componente que irá receber as mensagens
    private JTextArea usuariosConectados; //Mostra os usuários onlines
    private Scanner entrada;

    public ThreadChat(Scanner entrada, JTextArea chat, JTextArea usuariosConectados) {
        this.usuariosConectados = usuariosConectados;
        this.entrada = entrada;
        this.chat = chat;
    }

    @Override
    public void run() {
        String msg;

        while (entrada.hasNext()) {
            //Guarda a mensagem recebida
            msg = entrada.nextLine();

            //Se for uma mensagem para atualizar os contatos...
            if (msg.equals("-atualizar contatos-")) {
                String nomesContatos = entrada.next();//Pega os nomes
                System.out.println(nomesContatos);
                atualizarContatos(nomesContatos);
                
                //Se não for...
            } else {                
                chat.append(msg + "\n\n");
            }
        }
    }

    private void atualizarContatos(String nomesContatos) {
        String[] nomes = nomesContatos.split("::");
        usuariosConectados.setText("");
        
        for(int i=0; i < nomes.length; i++){
            usuariosConectados.append(nomes[i] + "\n");
        }        
    }

}
