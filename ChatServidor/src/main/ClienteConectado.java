/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatservidor;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;
import java.io.PrintStream;
import java.net.Socket;

/**
 *
 * @author KONET
 */
public class ClienteConectado {
    private String apelido;
    private Socket socket;
    private Scanner entrada;
    private PrintStream saida;
    private String ip;

    public ClienteConectado(String apelido, Socket socket) throws IOException {
        this.apelido = apelido;
        this.socket = socket;
        
        this.entrada = new Scanner (socket.getInputStream());
        this.saida = new PrintStream(socket.getOutputStream());
        this.ip = socket.getInetAddress().getHostAddress();
    }

    public String getApelido() {
        return apelido;
    }

    public void setApelido(String apelido) {
        this.apelido = apelido;
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public String getIp() {
        return ip;
    }

    public Scanner getEntrada() {
        return entrada;
    }

    public PrintStream getSaida() {        
        return saida;
    }

    public void fecharConexao() throws IOException {
        this.entrada.close();
        this.saida.close();
        this.socket.close();
    }
    
    

    
    
}
