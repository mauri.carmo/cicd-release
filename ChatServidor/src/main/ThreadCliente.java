/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatservidor;

import java.io.IOException;
import java.net.Socket;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author KONET
 */
public class ThreadCliente extends Thread {

    private ClienteConectado cliente;
    private Scanner entrada;
    ClienteConectado cc;
    List<ClienteConectado> clientesConectados;

    public ThreadCliente(Socket socketCliente, List<ClienteConectado> clientesConectados) throws IOException {
        this.cliente = new ClienteConectado("", socketCliente);
        String apelido = this.cliente.getEntrada().nextLine(); //Pega o nome do usuário
        this.cliente.setApelido(apelido);
        this.clientesConectados = clientesConectados;
        this.clientesConectados.add(cliente);

        this.mostrarClienteConectado();
        this.start();
    }

    @Override
    public void run() {
        //Pega as mensagem de um cliente
        entrada = cliente.getEntrada();
        String msg;

        while (true) {
            //Pega a mensagem recebida
            msg = entrada.nextLine();
            //Se for a mensagem "desconecte-me", faz a desconexão (sai do laço)
            if (msg.equals("desconecte-me")) {
                break;
            } else {

                //Envia a mensagem pra todos os outros cliente conectados.
                enviarPTodos(msg);
            }
        }

        //Fechando a conexão...
        desconectarCliente();

    }

    private void mostrarClienteConectado() {
        System.out.println("Cliente " + cliente.getApelido() + " conectado - IP: " + cliente.getIp());
        enviarPTodos(cliente.getApelido() + " entrou no chat.");
        atualizarClientesConectados();
    }

    //Desconecta o cliente do servidor
    private void desconectarCliente() {
        try {
            clientesConectados.remove(cliente); //Remove o cliente
            cliente.fecharConexao();
            entrada.close();
            System.out.println("Cliente " + cliente.getApelido() + " desconectou-se!");
            enviarPTodos(cliente.getApelido() + " desconectou-se.");
            this.interrupt();
        } catch (IOException ex) {
            System.out.println("Erro ao fechar conexao");
        }
    }

    private void enviarPTodos(String msg) {
        for (int i = 0; i < clientesConectados.size(); i++) {
            cc = clientesConectados.get(i);
            if (cc != cliente) { //Não manda a mensagem para o cliente que a escreveu
                System.out.println("msg: " + msg);
                cc.getSaida().println(msg);
            }
        }
    }

    private void atualizarClientesConectados() {
        String nomesClientesConectados = "";

        //Monta a lista com o nome
        for (ClienteConectado cliente : clientesConectados) {
            nomesClientesConectados += cliente.getApelido() + "::";
        }

        //Envia uma flag e em seguida o nome dos usuarios conectados
        for (int i = 0; i < clientesConectados.size(); i++) {
            cc = clientesConectados.get(i);
            cc.getSaida().println("-atualizar contatos-");
            cc.getSaida().println(nomesClientesConectados);
        }
    }

}
