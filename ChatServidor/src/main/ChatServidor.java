/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatservidor;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author KONET
 */
public class ChatServidor {

    //Porta para conexão
    private static final int PORTA = 33123;
    
    private static ServerSocket servidor;
    private static Socket socketCliente;
    private static List<ClienteConectado> clientesConectados;
    public static void main(String[] args) {
        clientesConectados = new ArrayList<>();
        
        //Inicia o servidor na porta dada
        try {
            servidor = new ServerSocket(PORTA);
            System.out.println("Servidor iniciado!\nPorta: " + PORTA);
        } catch (Exception e) {
            System.out.println("Erro ao iniciar o servidor!\nErro: " + e.getMessage());
        }

        //Prepara a conexão para os clientes
        while(true){
            try {
                socketCliente = servidor.accept();
                new ThreadCliente(socketCliente, clientesConectados);
            } catch (IOException ex) {
                System.out.println("Erro ao abrir conexão!");
            }
        }
    }
        
}
